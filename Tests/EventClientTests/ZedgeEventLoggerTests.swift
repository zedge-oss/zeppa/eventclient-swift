
import Foundation

import XCTest
import Foundation
import PromiseKit
@testable import libnetwork_ios
@testable import EventClient


final class ZedgeEventLoggerTest: XCTestCase {
    var logger: ZedgeEventLogger!
    var session: NetworkSessionMock!
    override func setUp() {

        let sessionMock = NetworkSessionMock()
        logger = ZedgeEventLogger(session: sessionMock, endpoint: URL(string: "https://example.net/eventlogs")!)
        session = sessionMock
    }

    func testRetryFail() {
        enum Test: Error {
            case error
        }

        let expect = expectation(description: "Wait for retry")
        retry(times: 4, cooldown: 0) { () -> Promise<Void> in
            return Promise(error: Test.error)
        }.catch { error in
            expect.fulfill()
        }
        waitForExpectations(timeout: 1)
    }

    func testRetrySucceed() {
        enum Test: Error {
            case error
        }

        let expect = expectation(description: "Wait for retry")
        var count = 0
        retry(times: 4, cooldown: 0) { () -> Promise<Void> in
            count += 1
            if count == 5 {
                return Promise()
            }
            return Promise(error: Test.error)
        }
        .done { expect.fulfill() }
        .catch { error in
            XCTFail()
            expect.fulfill()
        }
        waitForExpectations(timeout: 1)
    }
    
    func testZedgeLoggingWithUserProperties() {
        logger.identify(TestUserProperties(isLoggedIn: true))
        logger.log(TestEvent(event: .startApp, age: 12))
       
        var events = getRequestBodyAsStruct()["events"] as! [[String: Any]]
        var first = events[0] as [String: Any]
        XCTAssertEqual(first["event"] as! String, "START_APP")
        var eventProperties = first["properties"] as! [String: Any]
        XCTAssertEqual(eventProperties["age"] as! Int, 12)
        XCTAssertTrue(eventProperties["is_logged_in"] as! Bool)
        XCTAssertNotNil(first["client_timestamp"])
        XCTAssertNotNil(first["event_dedupe_key"])
        
        logger.identify(TestUserProperties(isLoggedIn: false))
        logger.log(TestEvent(event: .startApp, age: 2))
        
        events = getRequestBodyAsStruct()["events"] as! [[String: Any]]
        first = events[0] as [String: Any]
        XCTAssertEqual(first["event"] as! String, "START_APP")
        eventProperties = first["properties"] as! [String: Any]
        XCTAssertEqual(eventProperties["age"] as! Int, 2)
        XCTAssertFalse(eventProperties["is_logged_in"] as! Bool)
        XCTAssertNotNil(first["client_timestamp"])
        XCTAssertNotNil(first["event_dedupe_key"])
        
        logger.log(TestEvent(event: .startApp, age: 3))
        events = getRequestBodyAsStruct()["events"] as! [[String: Any]]
        first = events[0] as [String: Any]
        XCTAssertEqual(first["event"] as! String, "START_APP")
        eventProperties = first["properties"] as! [String: Any]
        XCTAssertEqual(eventProperties["age"] as! Int, 3)
        XCTAssertFalse(eventProperties["is_logged_in"] as! Bool)
        XCTAssertNotNil(first["client_timestamp"])
        XCTAssertNotNil(first["event_dedupe_key"])
        
        logger.log(TestEvent(event: .identifyUser))
        events = getRequestBodyAsStruct()["events"] as! [[String: Any]]
        first = events[0] as [String: Any]
        XCTAssertEqual(first["event"] as! String, "IDENTIFY_USER")
        eventProperties = first["properties"] as! [String: Any]
        XCTAssertFalse(eventProperties["is_logged_in"] as! Bool)
        XCTAssertNotNil(first["client_timestamp"])
        XCTAssertNotNil(first["event_dedupe_key"])
    }

    private func getRequestBodyAsStruct() -> [String: Any] {
        return try! (JSONSerialization.jsonObject(with: Data(getRequestBody().utf8), options: []) as? [String: Any])!
    }

    private func getRequestBody() -> String {
        return String(data: (session.request?.httpBody!)!, encoding: .utf8)!
    }

    struct TestEvent: LoggableEvent {
        func getDefinedEnums() -> [Any] { EventName.allCases }

        var event: EventName?
        var age: Int? = nil
        var optionalString: String?
    }

    enum EventName: String, CaseIterable {
        case identifyUser
        case startApp
    }

    struct TestUserProperties: LoggableProperties {
        func getDefinedEnums() -> [Any] {
            return []
        }

        var isLoggedIn: Bool = false
    }
    
    func testStringVersionOfRequestBody() {
        logger.identify(TestUserProperties(isLoggedIn: true))
        logger.log(TestEvent(event: .startApp, age: 12, optionalString: "value"))

        XCTAssertTrue(getRequestBody().contains("\"optional_string\":\"value\""))
    }
}
