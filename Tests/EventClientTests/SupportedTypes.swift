import XCTest
@testable import EventClient

enum EventEnum : CaseIterable {
    case openApp, closeApp
}

// An enum from a library where you cannot make it caseiterable by yourself
enum LibraryEnum {
    case firstValue, secondValue, thirdValue
}

// Example of an event struct with all the supported types it can contain
struct SupportedEventTypes : LoggableEvent {
    func getDefinedEnums() -> [Any] {
        EventEnum.allCases +
            [LibraryEnum.firstValue, LibraryEnum.secondValue, LibraryEnum.thirdValue]
    }

    var event: EventEnum?

    var optionalEnum: LibraryEnum? = nil
    var optionalString: String? = nil
    var optionalBoolean: Bool? = nil
    var optionalByte: Int8? = nil
    var optionalShort: Int16? = nil
    var optionalInt: Int? = nil
    var optionalLong: Int64? = nil
    var optionalFloat: Float? = nil
    var optionalDouble: Double? = nil

    var optionalEnums: [LibraryEnum]? = nil
    var optionalStrings: [String]? = nil
    var optionalBooleans: [Bool]? = nil
    var optionalBytes: [Int8]? = nil
    var optionalShorts: [Int16]? = nil
    var optionalInts: [Int]? = nil
    var optionalLongs: [Int64]? = nil
    var optionalFloats: [Float]? = nil
    var optionalDoubles: [Double]? = nil

    var `enum`: LibraryEnum = .firstValue
    var string: String = ""
    var boolean: Bool = false
    var byte: Int8 = 1
    var short: Int16 = 1
    var int: Int = 1
    var long: Int64 = 1
    var float: Float = 1.0
    var double: Double = 1.0

    var enums: [LibraryEnum] = [.thirdValue]
    var strings: [String] = ["stringsValue"]
    var booleans: [Bool] = [false, true]
    var bytes: [Int8] = [1, 2, 3]
    var shorts: [Int16] = [4, 5, 6]
    var ints: [Int] = [7, 8, 9]
    var longs: [Int64] = [10, 11, 12]
    var floats: [Float] = [1.3, 1.4]
    var doubles: [Double] = [1.5, 1.6, 1.7]
}
