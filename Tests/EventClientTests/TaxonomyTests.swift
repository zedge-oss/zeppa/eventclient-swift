import XCTest
import PromiseKit
@testable import EventClient


import Foundation

extension Array where Element == [String: Any] {
    func get(name: String) -> [String: Any]? {
        for value in self {
            if value["name"] as? String == name {
                return value
            }
        }
        return nil
    }
}

@available(iOS 11.0, *)
final class TaxonomyTests: XCTestCase {

    func testTaxonomyRoundtrip() {
        var taxonomy = Taxonomy(appId: "awesomeapp.ios")
        try! taxonomy.update(properties: TestEvent(event: nil))
        let jsonData = try! JSONEncoder().encode(taxonomy)

        //let jsonDataAsString = String(data: jsonData, encoding: .utf8)!

        let jsonDecoder = JSONDecoder()
        let taxonomyRoundtrip = try! jsonDecoder.decode(Taxonomy.self, from: jsonData)

        XCTAssertEqual(taxonomyRoundtrip.appid, "awesomeapp.ios")
        XCTAssertEqual(taxonomyRoundtrip.enums.count,  2)
        XCTAssertEqual(taxonomyRoundtrip.properties.count, 4)
    }

    struct TestEvent: LoggableEvent {
        func getDefinedEnums() -> [Any] { EventName.allCases + Gender.allCases }
        
        var event: EventName?
        var name: String? = nil
        var age: Int? = nil
        var gender: Gender? = nil
    }

    enum EventName: String, CaseIterable {
        case identifyUser
        case startApp
    }

    enum Gender: CaseIterable
    {
        case man
        case woman
        case other
    }

    func testTaxonomyEnum() {
        let root = getJsonData(EventWithEnum())
        let enums = root["enums"] as! [[String: Any]]

        let genders = enums.get(name: "Gender")!["fields"] as! [[String:Any]]
        let multiEnumWords = enums.get(name: "MultiWordEnum")!["fields"] as! [[String: Any]]

        XCTAssertNotNil(genders.get(name: "MAN"))
        XCTAssertNotNil(genders.get(name: "WOMAN"))
        XCTAssertNotNil(genders.get(name: "OTHER"))

        XCTAssertNotNil(multiEnumWords.get(name: "RING_ZERO"))
        XCTAssertNotNil(multiEnumWords.get(name: "RING_1_DERP_FOO"))
        XCTAssertNotNil(multiEnumWords.get(name: "RING_2"))
        XCTAssertNotNil(multiEnumWords.get(name: "RING_DING_DING_DONG"))
        XCTAssertNotNil(multiEnumWords.get(name: "NO_ONE_WOULD_DEFINE_SUCH_AN_ENUM"))

        let properties = root["properties"] as! [[String: Any]]
        XCTAssertEqual(properties.get(name: "gender")!["type"] as! String, "Enum:Gender")
        XCTAssertEqual(properties.get(name: "best_friend_gender")!["type"] as! String, "Enum:Gender")
        XCTAssertEqual(properties.get(name: "multi_word_enum")!["type"] as! String, "Enum:MultiWordEnum")
    }

    func getJsonData(_ event : LoggableEvent) -> [String: Any] {
        return try! tryGetJsonData(event)
    }

    func tryGetJsonData(_ event : LoggableEvent) throws -> [String: Any] {
        let jsonData = try toJson(from: event)
        let rootObject = try? JSONSerialization.jsonObject(with: jsonData, options: [])
        return rootObject as! [String: Any]
    }
    
    func toJson(from event: LoggableEvent) throws -> Data {
        var taxonomy = Taxonomy(appId: "kake")
        try taxonomy.update(properties: event)
        let taxonomyEncoder = JSONEncoder()
        taxonomyEncoder.outputFormatting = [.sortedKeys , .prettyPrinted]
        return try! taxonomyEncoder.encode(taxonomy)
    }

    struct EventWithEnum: LoggableEvent {
        let event: String = "dummyEventName"
        func getDefinedEnums() -> [Any] { Gender.allCases + MultiWordEnum.allCases }

        var gender : Gender? = nil
        var bestFriendGender : Gender? = nil
        var multiWordEnum : MultiWordEnum? = nil
    }
    
    enum MultiWordEnum : CaseIterable {
        case ringZero
        case ring1_derpFoo
        case ring2
        case ringDingDingDong
        case no_ONEwould_defineSUCH_anEnum
    }
    
    struct EventAndDifferentScopes: LoggableEvent {
        func getDefinedEnums() -> [Any] {
            Gender.allCases + EventName.allCases
        }
        
        var event: EventName?
        var name: String? = nil
        var age: Int? = nil
        var gender: Gender? = nil
        
        func getScope(of variable: String) -> Scope {
              switch (variable) {
              case "event": return .envelope
              case "gender": return .user
              case "age": return .internal
              default: return .event
              }
        }
    }
    
    func testTaxonomyScopes() {
        let testData = EventAndDifferentScopes()
        let root = getJsonData(testData)

        let properties = root["properties"] as! [[String: Any]]
        XCTAssertEqual(properties.get(name: "event")!["scope"] as! String, "Envelope")
        XCTAssertEqual(properties.get(name: "name")!["scope"] as! String, "Event")
        XCTAssertEqual(properties.get(name: "age")!["scope"] as! String, "Internal")
        XCTAssertEqual(properties.get(name: "gender")!["scope"] as! String, "User")
    }
    

    func testTaxonomyNatives() {
        let root = getJsonData(EventWithNatives())

        let properties = root["properties"] as! [[String: Any]]
        XCTAssertEqual(properties.get(name: "string")!["type"] as! String, "String")
        XCTAssertEqual(properties.get(name: "longer_string_name")!["type"] as! String, "String")
        XCTAssertEqual(properties.get(name: "int_32")!["type"] as! String, "Int32")
        XCTAssertEqual(properties.get(name: "int")!["type"] as! String, "Int32")
        XCTAssertEqual(properties.get(name: "int_64")!["type"] as! String, "Int64")
        XCTAssertEqual(properties.get(name: "float")!["type"] as! String, "Float32")
        XCTAssertEqual(properties.get(name: "float_32")!["type"] as! String, "Float32")
        XCTAssertEqual(properties.get(name: "float_64")!["type"] as! String, "Float64")
        XCTAssertEqual(properties.get(name: "double")!["type"] as! String, "Float64")
        XCTAssertEqual(properties.get(name: "bool")!["type"] as! String, "Bool")
    }

    struct EventWithNatives: LoggableEvent {
        func getDefinedEnums() -> [Any] { [] }

        let event =  "dummyEventName"

        var string : String? = nil
        var longerStringName: String? = nil
        var int : Int? = nil
        var int32: Int32? = nil
        var int64: Int64? = nil
        var float : Float? = nil
        var float32 : Float32? = nil
        var float64 : Float64? = nil
        var double : Double? = nil
        var bool : Bool? = nil
    }

    func testTaxonomyArrayNatives() {
        let root = getJsonData(EventWithArrayNatives())

        let properties = root["properties"] as! [[String: Any]]
        XCTAssertEqual(properties.get(name: "string")!["type"] as! String, "Array(String)")
        XCTAssertEqual(properties.get(name: "some_longer_name")!["type"] as! String, "Array(String)")
        XCTAssertEqual(properties.get(name: "int_32")!["type"] as! String, "Array(Int32)")
        XCTAssertEqual(properties.get(name: "int")!["type"] as! String, "Array(Int32)")
        XCTAssertEqual(properties.get(name: "int_64")!["type"] as! String, "Array(Int64)")
        XCTAssertEqual(properties.get(name: "float")!["type"] as! String, "Array(Float32)")
        XCTAssertEqual(properties.get(name: "float_32")!["type"] as! String, "Array(Float32)")
        XCTAssertEqual(properties.get(name: "float_64")!["type"] as! String, "Array(Float64)")
        XCTAssertEqual(properties.get(name: "double")!["type"] as! String, "Array(Float64)")
        XCTAssertEqual(properties.get(name: "bool")!["type"] as! String, "Array(Bool)")
    }

    struct EventWithArrayNatives: LoggableEvent {
        func getDefinedEnums() -> [Any] { [] }

        let event = "dummyEventName"
        var someLongerName : [String]? = nil
        var string : [String]? = nil
        var int : [Int]? = nil
        var int32: [Int32]? = nil
        var int64: [Int64]? = nil
        var float : [Float]? = nil
        var float32 : [Float32]? = nil
        var float64 : [Float64]? = nil
        var double : [Double]? = nil
        var bool : [Bool]? = nil
    }

    func testTaxonomyArrayEnum() {
        let root = getJsonData(EventWithArrayEnum())

        let enums = root["enums"] as! [[String: Any]]

        XCTAssertEqual(enums[0]["name"] as! String, "Scopes")
        let enumFields = enums[0]["fields"] as! [[String:Any]]
        XCTAssertEqual(enumFields[0]["name"] as! String, "EMAIL")
        XCTAssertEqual(enumFields[1]["name"] as! String, "NAME")

        let properties = root["properties"] as! [[String: Any]]
        XCTAssertEqual(properties[0]["name"] as! String, "scopes")
        XCTAssertEqual(properties[0]["type"] as! String, "Array(Enum:Scopes)")
    }

    enum Scopes : CaseIterable {
       case email
       case name
    }

    struct EventWithArrayEnum: LoggableEvent {
       func getDefinedEnums() -> [Any] { Scopes.allCases }

       var scopes : [Scopes]? = nil
    }

    func testTaxonomyEnumNotDefined() {
        do {
            let _ = try tryGetJsonData(EventWithMissingDefinedEnumsThrowsException())
            XCTFail("Generation of taxonomy should fail, due to using Enum without listing it in definedEnums!")
        }catch(TaxonomyError.undefined_enum) {
            XCTAssertTrue(true)
        }catch {
            XCTFail("Wrong exception")
        }
    }

    struct EventWithMissingDefinedEnumsThrowsException : LoggableEvent {
        func getDefinedEnums() -> [Any] { [] }
        let event = "dummyEventName"
        var gender : Gender? = nil
    }

    func testTaxonomyArrayInception() {
        do {
            let _ = try tryGetJsonData(EventWithInvalidArrayInception())
            XCTFail("Generation of taxonomy should fail!")
        }catch(EventClient.TaxonomyError.unsupported_multi_dimensions) {
            XCTAssertTrue(true)
        }catch {
            XCTFail("Wrong exception")
        }

        do {
            let _ = try tryGetJsonData(EventWithInvalidArrayInceptionEnum())
            XCTFail("Generation of taxonomy should fail!")
        }catch(EventClient.TaxonomyError.unsupported_multi_dimensions) {
            XCTAssertTrue(true)
        }catch {
            XCTFail("Wrong exception")
        }
    }

    struct EventWithInvalidArrayInception : LoggableEvent {
        func getDefinedEnums() -> [Any] { [] }
        let event = "dummyEventName"
        var foo : [[String]]? = nil
    }

    struct EventWithInvalidArrayInceptionEnum : LoggableEvent {
        func getDefinedEnums() -> [Any] { Gender.allCases }
        let event = "dummyEventName"
        var gender : [[Gender]]? = nil
    }

    static var allTests = [
        ("testTaxonomyRoundtrip", testTaxonomyRoundtrip),
        ("testTaxonomyEnum", testTaxonomyEnum),
        ("testTaxonomyNatives", testTaxonomyNatives),
        ("testTaxonomyArrayNatives", testTaxonomyArrayNatives),
        ("testTaxonomyEnumArray", testTaxonomyArrayEnum),
        ("testTaxonomyEnumNotDefined", testTaxonomyArrayEnum),
        ("testTaxonomyArrayInception", testTaxonomyArrayInception),
    ]
}
