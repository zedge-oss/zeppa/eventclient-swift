import XCTest
import Foundation
@testable import EventClient

internal extension Dictionary where Key == String, Value == Any {
    func getEventScope() -> [String: Any] {
        return self["properties"] as! [String: Any]
    }
}

final class EventGateTests: XCTestCase {
    var recorder: EventRecorder!
    var logger: EventGate!

    override func setUp() {
        recorder = EventRecorder()
        logger = EventGate(queueSize: 1024,
                           logger: recorder) { overflowCount in
                            self.recorder.log(
                                TestEvent(event: .loseEventsFromQueueLimit, count: overflowCount))
        }
    }

    struct TestEvent: LoggableEvent {
        func getDefinedEnums() -> [Any] { EventName.allCases }

        var event: EventName?
        var age: Int? = nil
        var count: Int? = nil
    }

    struct TestUserProperties: LoggableProperties {
        func getDefinedEnums() -> [Any] {
            return []
        }

        var isLoggedIn: Bool = false
    }

    enum EventName: String, CaseIterable {
        case identifyUser
        case startApp
        case suspendApp
        case loseEventsFromQueueLimit
        case clickEvent
    }

    func testBufferingLogs() {
        logger.log(TestEvent(event: .startApp))
        XCTAssertEqual(recorder.events.count, 0)

        logger.open()
        logger.log(TestEvent(event: .suspendApp))
        XCTAssertEqual(recorder.events.count, 2)

        logger.log(TestEvent(event: .startApp))
        XCTAssertEqual(recorder.events.count, 3)

        logger.close()

        logger.log(TestEvent(event: .suspendApp))
        XCTAssertEqual(recorder.events.count, 3)
        XCTAssertEqual(recorder.events[0].getZedgeProperties()["event"] as! String, "START_APP")
        XCTAssertEqual(recorder.events[1].getZedgeProperties()["event"] as! String, "SUSPEND_APP")
        XCTAssertEqual(recorder.events[2].getZedgeProperties()["event"] as! String, "START_APP")
    }

    func testBufferingUserProperties() {
        logger.identify(TestUserProperties(isLoggedIn: false))
        logger.log(TestEvent(event: .suspendApp))

        XCTAssertEqual(recorder.userProperties.count, 0)

        logger.open()
        logger.identify(TestUserProperties(isLoggedIn: true))
        XCTAssertEqual(recorder.userProperties.count, 2)

        logger.identify(TestUserProperties(isLoggedIn: false))
        XCTAssertEqual(recorder.userProperties.count, 3)


    }

    func testLimitingSize() {
        logger = EventGate(queueSize: 3, logger: recorder) { numberOverflow in self.recorder.log( TestEvent(event: .loseEventsFromQueueLimit, count: numberOverflow))
        }
        logger.log(TestEvent(event: .startApp, age: 12)) // ignored
        logger.identify(TestUserProperties(isLoggedIn: false)) // ignored
        logger.identify(TestUserProperties(isLoggedIn: true))
        logger.log(TestEvent(event: .suspendApp))
        logger.log(TestEvent(event: .clickEvent))
        logger.open()
        logger.log(TestEvent(event: .startApp))
        XCTAssertEqual(recorder.events[0].getProperties()["event"] as! String? , "LOSE_EVENTS_FROM_QUEUE_LIMIT")
        XCTAssertEqual(recorder.events[0].getProperties().getEventScope()["count"] as! Int? , 2)
        XCTAssertEqual(recorder.events[1].getZedgeProperties()["event"] as! String?, "SUSPEND_APP")
        XCTAssertEqual(recorder.userProperties[0].getProperties().getEventScope()["is_logged_in"] as! Bool?, true)
        XCTAssertEqual(recorder.events[2].getZedgeProperties()["event"] as! String?, "CLICK_EVENT")
    }

    func testReadyShiftingOnAndOff_GivesCorrectOverflowCount() {
        let sizeLimit = 2
        logger = EventGate(queueSize: sizeLimit, logger: recorder) { numberOverflow in self.recorder.log( TestEvent(event: .loseEventsFromQueueLimit, count: numberOverflow))
        }
        for _ in 1...sizeLimit+1 {
            for _ in 1...sizeLimit+1 {
                logger.log(TestEvent(event: .suspendApp))
            }
            logger.open()
            logger.log(TestEvent(event: .startApp))
            print("Events recorded count #  ", recorder.events.count)
            XCTAssertEqual(recorder.events[0].getProperties()["event"] as! String, "LOSE_EVENTS_FROM_QUEUE_LIMIT")
            XCTAssertEqual(recorder.events.last?.getZedgeProperties()["event"] as! String, "START_APP")
            recorder.clear()
            logger.close()
        }
    }

    func testOverflowCount() {
        let sizeLimit = 2
        logger = EventGate(queueSize: sizeLimit, logger: recorder) { numberOverflow in self.recorder.log( TestEvent(event: .loseEventsFromQueueLimit, count: numberOverflow))
        }
        for _ in 1...sizeLimit+5 {
            logger.log(TestEvent(event: .suspendApp))
        }
        logger.open()
        XCTAssertEqual(recorder.events[0].getZedgeProperties()["event"] as! String, "LOSE_EVENTS_FROM_QUEUE_LIMIT")
        print(recorder.events[0].getZedgeProperties())
        XCTAssertEqual(recorder.events[0].getProperties().getEventScope()["count"] as! Int, 5)
    }

    func testFlushOnOpen() {
        logger.log(TestEvent(event: .startApp))
        XCTAssertEqual(recorder.events.count, 0)
        XCTAssertEqual(recorder.userProperties.count, 0)
        logger.open()
        XCTAssertEqual(recorder.events.count, 1)
        XCTAssertEqual(recorder.userProperties.count, 0)
    }
}
