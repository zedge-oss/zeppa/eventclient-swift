import XCTest
import PromiseKit
@testable import EventClient

public class EventRecorder : EventLogger {
    enum EventType {
        case event
        case userProperties
    }
    var events = [LoggableEvent]()
    var userProperties = [LoggableProperties]()
    var orderedBuffer = [EventType]()

    public func log(_ event: LoggableEvent) {
        events.append(event)
        orderedBuffer.append(.event)
    }

    public func identify(_ user: LoggableProperties) {
        userProperties.append(user)
        orderedBuffer.append(.userProperties)
    }

    public func clear() {
        events.removeAll()
        userProperties.removeAll()
        orderedBuffer.removeAll()
    }

}

internal class EventPipeline {
    static var logger: EventLogger = EventGate(queueSize: 1024, logger: NullEventLogger()) { overflowCount in }
}

internal func log(_ event: LoggableEvent) {
    EventPipeline.logger.log(event)
}

internal func identify(_ properties: LoggableProperties) {
    EventPipeline.logger.identify(properties)
}

@available(OSX 10.13, *)
final class EventClientTests: XCTestCase {
    let recorder = EventRecorder()
    override func setUp() {
        EventPipeline.logger = recorder
    }

    func testZedgeLogging() {
        log(TestEvent(event: .identifyUser, age: 12))
        let properties = recorder.events[0].getZedgeProperties()
        XCTAssertEqual(properties["event"] as! String, "IDENTIFY_USER")
        let eventScopedProperties = properties.getEventScope()
        XCTAssertEqual(eventScopedProperties["age"] as! Int, 12)
        XCTAssertNil(properties["age"])
    }

    func testZedgeLoggingWithUserPropertiesAsEnvelope() {
        identify(TestUserProperties(userId: "my_id"))
        log(TestEvent(event: .identifyUser, age: 12))
        let properties = recorder.events[0].getZedgeProperties()
        XCTAssertEqual(properties["event"] as! String, "IDENTIFY_USER")
        let eventScopedProperties = properties.getEventScope()
        XCTAssertEqual(eventScopedProperties["age"] as! Int, 12)
        XCTAssertNil(properties["age"])
        let userProperties = recorder.userProperties[0].getProperties().getEventScope()
        XCTAssertEqual(userProperties["user_id"] as! String, "my_id")
    }

    func testZedgeLoggingScopeOverride() {
        log(TestEventWithScopeOverride(event: .identifyUser, age: 12, wishingNumberAsEnvelope: 42))
        let properties = recorder.events[0].getZedgeProperties()
        XCTAssertEqual(properties["event"] as! String, "IDENTIFY_USER")
        let eventScopedProperties = properties["properties"] as! [String: Any]
        XCTAssertEqual(properties["wishing_number_as_envelope"] as! Int, 42)
        XCTAssertNil(eventScopedProperties["wishing_number_as_envelope"])
    }

    struct TestEventWithScopeOverride : LoggableEvent {
       func getDefinedEnums() -> [Any] { EventName.allCases }

        // Overrides default implemention which only promotes "event" to .Envelope
        // Promotion to .Envelope properties is usually only done by the library
        // as this is a routing mechanic for the event pipeline.
        func getScope(of variable: String) -> Scope {
            switch (variable) {
               case "event": return .envelope
               case "wishingNumberAsEnvelope": return .envelope
               default: return .event
            }
        }

        var event: EventName?
        var age: Int? = nil
        var wishingNumberAsEnvelope: Int? = nil
    }

    struct TestEvent: LoggableEvent {
        func getDefinedEnums() -> [Any] { EventName.allCases }

        var event: EventName?
        var age: Int? = nil
    }

    enum EventName: String, CaseIterable {
        case identifyUser
        case startApp
    }

    struct TestUserProperties: LoggableProperties {
        func getDefinedEnums() -> [Any] {
            return []
        }

        var isLoggedIn: Bool = false
        var userId: String? = nil
    }

    func testAllSupportedPropertyTypes() {
        log(SupportedEventTypes(
            event: .openApp,
            optionalEnum: .secondValue,
            optionalString: "stringValue",
            optionalBoolean: true,
            optionalByte: 127,
            optionalDouble: 2.0,
            optionalEnums: [.secondValue, .thirdValue],
            optionalStrings: ["firstString", "secondString"],
            optionalBooleans: [true, false],
            optionalLongs: [1_000_000, 0]
        ))
        let properties = recorder.events[0].getAmplitudeProperties()
        XCTAssertEqual(properties["Event"] as! String, "Open App")
        XCTAssertEqual(properties["Optional Enum"] as! String, "Second Value")
        XCTAssertEqual(properties["Optional String"] as! String, "stringValue")
        XCTAssertEqual(properties["Optional Boolean"] as! Bool, true)
        XCTAssertEqual(properties["Optional Byte"] as! Int8, 127)
        XCTAssertEqual(properties["Optional Double"] as! Float64, 2.0)
        XCTAssertEqual(properties["Optional Enums"] as! [String], ["Second Value", "Third Value"])
        XCTAssertEqual(properties["Optional Strings"] as! [String], ["firstString", "secondString"])
        XCTAssertEqual(properties["Optional Booleans"] as! [Bool], [true, false])
        XCTAssertEqual(properties["Optional Longs"] as! [Int64], [1_000_000, 0])
        XCTAssertEqual(properties["Boolean"] as! Bool, false)
        XCTAssertEqual(properties["Enums"] as! [String], ["Third Value"])
        XCTAssertEqual(properties["Strings"] as! [String], ["stringsValue"])

        XCTAssertNil(properties["Optional Bytes"])
    }
}

extension String {
    func base64Encoded() -> String? {
        return data(using: .utf8)?.base64EncodedString()
    }

    func base64Decoded() -> String? {
        var st = self;
        if (self.count % 4 <= 2){
            st += String(repeating: "=", count: (self.count % 4))
        }
        guard let data = Data(base64Encoded: st) else { return nil }
        return String(data: data, encoding: .utf8)
    }
}
