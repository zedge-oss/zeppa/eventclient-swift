import Foundation
import StringCase

public enum TaxonomyError:  Error {
    case unsupported_multi_dimensions
    case undefined_enum
}

public enum Scope : String, CaseIterable, Codable {
    case envelope = "Envelope"
    case user = "User"
    case event = "Event"
    case `internal` = "Internal"
}

struct PropertyFields : Codable {
    var name : String
    var type : String
    var scope : Scope = .event
}

struct EnumFields : Codable{
    var name : String
    var suggestedValue : Int?
}

struct EnumClass : Codable {
    var name : String
    var fields : [EnumFields]
}


public struct Taxonomy : Codable {
    var appid: String = "demoapp.ios"
    var enums : [EnumClass] = []
    var properties : [PropertyFields] = []

    public init(appId: String) {
        self.appid = appId
    }

    public mutating func update(properties: LoggableProperties) throws {
        let mapper = ZedgeEventMapper()

        var enumsMap: [String: [EnumFields]] = [:]

        properties.getDefinedEnums().forEach { e in
            let enumName = String(describing: type(of: e)).camelized.uppercasingFirst

            if var exists = enumsMap[enumName] {
                exists.append(EnumFields(name: mapper.map(enum: e)))
                enumsMap[enumName] = exists
                // print("Append: \(enumName) -> \(e)")
            } else {
                enumsMap[enumName] = [EnumFields(name: mapper.map(enum: e))]
                // print("Create: \(enumName) -> \(e)")
            }
            //print("\(enumName) -> \(e)")
        }

        for (eventClass, enumProperties) in enumsMap {
            print("\(eventClass) -> \(enumProperties)")
            enums.append(EnumClass(name: eventClass, fields: enumProperties))
        }

        for case let (variableName?, value) in Mirror(reflecting: properties).children {
            let propertyType = String(describing: type(of: value))
            let valueType = type(of: value)
            let propertyScope = properties.getScope(of: variableName)
            let propertyName = mapper.map(property: variableName)
            switch valueType {
            case is Array<String>?.Type:
                self.properties.append(PropertyFields(
                    name: propertyName,
                    type: "Array(String)",
                    scope: propertyScope))
            case is String.Type:
                fallthrough
            case is String?.Type:
                self.properties.append(PropertyFields(
                    name: propertyName,
                    type: "String",
                    scope: propertyScope))
            case is Array<Int64>?.Type:
                self.properties.append(PropertyFields(
                    name: propertyName,
                    type: "Array(Int64)",
                    scope: propertyScope))
            case is Int64?.Type:
                self.properties.append(PropertyFields(
                    name: propertyName,
                    type: "Int64",
                    scope: propertyScope))
            case is Array<Int32>?.Type:
                fallthrough
            case is Array<Int>?.Type:
                self.properties.append(PropertyFields(
                    name: propertyName,
                    type: "Array(Int32)",
                    scope: propertyScope))
            case is Int32?.Type:
                fallthrough
            case is Int?.Type:
                self.properties.append(PropertyFields(
                    name: propertyName,
                    type: "Int32",
                    scope: propertyScope))
            case is Array<Float64>?.Type:
                self.properties.append(PropertyFields(
                    name: propertyName,
                    type: "Array(Float64)",
                    scope: propertyScope))
            case is Float64?.Type:
                self.properties.append(PropertyFields(
                    name: propertyName,
                    type: "Float64",
                    scope: propertyScope))
            case is Array<Double>?.Type:
                self.properties.append(PropertyFields(
                    name: propertyName,
                    type: "Array(Double32)",
                    scope: propertyScope))
            case is Double?.Type:
                self.properties.append(PropertyFields(
                    name: propertyName,
                    type: "Double32",
                    scope: propertyScope))
            case is Array<Float32>?.Type:
                fallthrough
            case is Array<Float>?.Type:
                self.properties.append(PropertyFields(
                    name: propertyName,
                    type: "Array(Float32)",
                    scope: propertyScope))
            case is Float32?.Type:
                fallthrough
            case is Float?.Type:
                self.properties.append(PropertyFields(
                    name: propertyName,
                    type: "Float32",
                    scope: propertyScope))
            case is Bool?.Type:
                self.properties.append(PropertyFields(
                    name: propertyName,
                    type: "Bool",
                    scope: propertyScope))
            case is Array<Bool>?.Type:
                self.properties.append(PropertyFields(
                    name: propertyName,
                    type: "Array(Bool)",
                    scope: propertyScope))
            default:
                var crocodiles = 0
                for chr in propertyType {
                    if chr == "<" || chr == ">" {
                        crocodiles += 1
                    }
                }
                // Cover Optional and Array.
                if crocodiles > 0 && crocodiles != 2 && crocodiles != 4 {
                    // "EventClient Library bug? This does not look like an enum. Bailing out for generating taxonomy! propertyType: \(propertyType) => enumType: \(enumType). \(crocodiles) crocodiles found. "
                    throw TaxonomyError.unsupported_multi_dimensions
                }

                let enumType : String
                let enumClass = propertyType
                    .replacingOccurrences(of: "Optional<", with: "")
                    .replacingOccurrences(of: "Array<", with: "")
                    .replacingOccurrences(of: ">", with: "")

                let keyExists = enumsMap[enumClass] != nil
                if !keyExists {
                    throw TaxonomyError.undefined_enum
                }

                if propertyType.contains("Array<") {
                    enumType = "Array(Enum:\(enumClass))"
                } else {
                    enumType = "Enum:\(enumClass)"
                }

                self.properties.append(PropertyFields(
                    name: propertyName,
                    type: enumType,
                    scope: propertyScope))
            }
        }
    }
}

fileprivate let badChars = CharacterSet.alphanumerics.inverted

extension String {
    var uppercasingFirst: String {
        return prefix(1).uppercased() + dropFirst()
    }

    var lowercasingFirst: String {
        return prefix(1).lowercased() + dropFirst()
    }

    var camelized: String {
        guard !isEmpty else {
            return ""
        }

        let parts = self.components(separatedBy: badChars)

        let first = String(describing: parts.first!).lowercasingFirst
        let rest = parts.dropFirst().map({String($0).uppercasingFirst})

        return ([first] + rest).joined(separator: "")
    }
}

extension String {

    var isLowercase: Bool {
        return self == self.lowercased()
    }

    var isUppercase: Bool {
        return self == self.uppercased()
    }

}
