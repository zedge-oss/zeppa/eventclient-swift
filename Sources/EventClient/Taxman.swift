
import Foundation
import libnetwork_ios
import PromiseKit

enum TaxmanErrors : Error {
    case failedToSerializeTaxonomy
}

public struct Taxman {
    let encoder: JSONEncoder = JSONEncoder()

    let session: NetworkSession

    var endpoint : String

    public init(endpoint: String, session: NetworkSession = URLSession.shared) {
        self.endpoint = endpoint
        self.session = session
    }
    
//URLSession.shared.dataTask(with: signedRequest) { (data, urlResponse, error) in
    public func update(taxonomy: Taxonomy) -> Promise<(data: Data, response: URLResponse)> {
        guard let data = try? JSONEncoder().encode(taxonomy) else {
            return Promise(error: TaxmanErrors.failedToSerializeTaxonomy)
        }

        var request = URLRequest(url:
            URL(string: endpoint)!)
        request.httpMethod = "PUT"
        request.httpBody = data
        
        return session.loadData(with: request)
    }

}
