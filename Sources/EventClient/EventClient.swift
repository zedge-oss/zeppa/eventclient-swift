import Foundation


public protocol LoggableEvent : LoggableProperties {
}

public protocol LoggableProperties {
    func getDefinedEnums() -> [Any]
    func getScope(of variable: String) -> Scope
}

public protocol EventMapper {
    func map(enum: Any) -> String
    func map(property: String) -> String
    func map(scope: Scope) -> Scope
}

public class ZedgeEventMapper: EventMapper {
    public init() { }
    public func map(enum: Any) -> String { String(describing: `enum`).snakeCased(.upper) }
    public func map(property: String) -> String { property.snakeCased(.lower) }
    public func map(scope: Scope) -> Scope { scope }
}

public class AmplitudeEventMapper: EventMapper {
    public func map(enum: Any) -> String {
        map(property: String(describing: `enum`))
    }
    public func map(property: String) -> String {
        property.snakeCased(.capitalized).replacingOccurrences(of: "_", with: " ")
    }
    public func map(scope: Scope) -> Scope { .envelope }
}

public class FirebaseEventMapper: EventMapper {
    public func map(enum: Any) -> String {
        String(describing: `enum`).snakeCased(.capitalized).replacingOccurrences(of: "_", with: " ")
    }
    public func map(property: String) -> String { property.snakeCased(.lower) }
    public func map(scope: Scope) -> Scope { .envelope }
}

public extension LoggableProperties {
    func getScope(of variable: String) -> Scope { .user }
    func getProperties(mapper: EventMapper = ZedgeEventMapper()) -> [String: Any] {
        return Mirror(reflecting: self).children.reduce(into: [String: Any]()) { (result, child) in
            let mirror = Mirror(reflecting: child.value)
            let property = child.label!
            let scope = mapper.map(scope: getScope(of: property))
            let key = mapper.map(property: property)
            if (mirror.displayStyle == .optional) {
                if (mirror.children.count != 0) {
                    result.insertScoped(key: key, value: mapper.map(value: mirror.children.first!.value), scope: scope)
                }
            } else {
                result.insertScoped(key: key, value: mapper.map(value: child.value), scope: scope)
            }
        }
    }

    func getZedgeProperties() -> [String: Any] {
        return getProperties(mapper: ZedgeEventMapper())
    }

    func getAmplitudeProperties() -> [String: Any] {
        return getProperties(mapper: AmplitudeEventMapper())
    }

    func getFirebaseProperties() -> [String: Any] {
        return getProperties(mapper: FirebaseEventMapper())
    }
}

private extension EventMapper {
    func map(value: Any) -> Any {
        if let array = value as? [Any] {
            if (array.count > 0 && isEnum(array[0])) {
                return array.map { map(enum: $0) }
            } else {
                return value
            }
        }
        if isEnum(value) {
            return map(enum: value)
        }
        return value
    }

    private func isEnum(_ value: Any) -> Bool {
        return Mirror(reflecting: value).displayStyle == .enum
    }
}

private extension Dictionary where Key == String, Value == Any {
    mutating func insertScoped(key: String, value: Any, scope: Scope) {
        if scope == .envelope {
            self[key] = value
        } else {
            if var properties = self["properties"] as? [String: Any] {
                properties[key] = value
                self["properties"] = properties
            } else {
                self["properties"] = [key: value]
            }
        }
    }
}

public extension LoggableEvent {
    func getScope(of variable: String) -> Scope {
        switch (variable) {
        case "event": return .envelope
        default: return .event
        }
    }
}

public protocol EventLogger {
    func log(_ event: LoggableEvent)
    func identify(_ user: LoggableProperties)
}

public class NullEventLogger: EventLogger {
    public func log(_ event: LoggableEvent) {
    }

    public func identify(_ user: LoggableProperties) {
    }
}
