import Foundation
import PromiseKit
import PMKFoundation
import libnetwork_ios

public final class ZedgeEventLogger: EventLogger {
    private let session: NetworkSession
    private let endpoint: URL

    private var userProperties: [String: Any] = [:]
    private var events: [[String: Any]] = []

    /**
     Dispatch loggable events to Zedge Log Sink

     - parameters:
        - session: HTTP transport protocol
        - endpoint: service URL annotated with app id and build
        - requestAdapter: Zedge request signer
     */
    public init(session: NetworkSession = URLSession.shared,
                endpoint: URL) {
        self.session = session
        self.endpoint = endpoint
    }

    public func log(_ event: LoggableEvent) {
        // Dispatch to Async DispatchQueue
        // events.append(event.getZedgeProperties())

        var zedgeEvents = event.getZedgeProperties()
        // Decorate user properties, but sadly we cannot inline this for some reason...
        zedgeEvents.overWrite(from: userProperties)
       
        let clientNow = Int64(NSDate().timeIntervalSince1970 * 1000.0)
        let randomInt = Int16.random(in: 1..<Int16.max) //Int.random(in: 1..<Int16.max)
        let internalProperties: [String : Any] = [
            "client_timestamp": clientNow,
            "event_dedupe_key": randomInt,
        ]
        zedgeEvents.overWrite(from: internalProperties)
        
        // Check if value is copied, not referenced
        let postData: [String: Any] = ["events": [zedgeEvents]]

        do {
            let bodyData = try JSONSerialization.data(withJSONObject: postData, options: [.sortedKeys])
            try dispatchEventsWithRetry(data: bodyData)
        } catch {
            debugPrint(error)
        }
    }

    private func dispatchEventsWithRetry(data: Data) throws {
        var request = URLRequest(url: endpoint)
        request.httpMethod = "POST"
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        request.httpBody = data

        retry(times: 3, cooldown: 2) {
            self.session.loadData(with: request)
                .validate()
        }
        .done { debugPrint(String(data: $0.data, encoding: .utf8) ?? "") }
        .catch {
            if case let PMKHTTPError.badStatusCode(statusCode, data, _) = $0 {
                debugPrint(statusCode)
                debugPrint(data)
            }
            debugPrint($0.localizedDescription)
            
        }
    }

    public func identify(_ user: LoggableProperties) {
        self.userProperties.overWrite(from: user.getZedgeProperties())
    }
}

extension Dictionary where Key == String, Value == Any {

    mutating func overWrite(from other: [String: Any]) {
        merge(other) { (existing, new) in
            if var existing = existing as? [String: Any], let newDict = new as? [String: Any] {
                existing.overWrite(from: newDict)
                return existing
            }
            return new }
    }
}
