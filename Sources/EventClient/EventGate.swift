
import Foundation
import os

/*
 RingBuffer inspired from https://github.com/raywenderlich/swift-algorithm-club/tree/master/Ring%20Buffer
 Allows overwriting data at beginning of ring.
 */
public struct RingBuffer<T> {
    fileprivate var array: [T?]
    fileprivate var writeIndex = 0

    public init(count: Int) {
        array = [T?](repeating: nil, count: count)
    }

    public mutating func write(_ element: T) {
        array[writeIndex % array.count] = element
        writeIndex += 1
    }

    public mutating func reapAll() -> (events: [T?], numberOfEventsOverflowed: Int) {
        var overFlow = writeIndex - array.count
        defer {
            array = [T?](repeating: nil, count: array.count)
            writeIndex = 0
        }
        return (array, overFlow)
    }
}

// Queue up events until open() is called.
// Events are flushed when open() is called.
public class EventGate: EventLogger {
    var logger: EventLogger
    var isOpen: Bool = false

    var buffer: RingBuffer<LoggableProperties>
    var onOverflowEvent: (Int) -> Void

    static let debug = OSLog(subsystem: "net.zedge.EventClient", category: "eventlogging")

    public init(queueSize: Int = 1024, logger: EventLogger, onOverflowEvent: @escaping (Int) -> Void) {
        buffer = RingBuffer<LoggableProperties>(count: queueSize)
        self.onOverflowEvent = onOverflowEvent
        self.logger = logger
    }

    public func open() {
        flush()
        isOpen = true
    }

    public func close() {
        isOpen = false
    }

    private func flush() {
        let (events, overFlowCount) = buffer.reapAll()
        if overFlowCount > 0 {
            onOverflowEvent(overFlowCount)
        }
        for e in events {
            if let event = e {
                switch event {
                case is LoggableEvent:
                    logger.log(event as! LoggableEvent)
                default:
                    logger.identify(event)
                }
            } else {
                break // End of events as of NOW.
            }
        }
    }

    public func log(_ event: LoggableEvent) {
        #if DEBUG
        let properties = event.getZedgeProperties()
        let debugDescription = "event: \(properties["event"] ?? ""), properties: \(properties["properties"] ?? "")"
        os_log("%{public}s", log: EventGate.debug, type: .debug, debugDescription)
        #endif
        if (isOpen) {
            logger.log(event)
        } else {
            enqueue(event)
        }
    }

    public func identify(_ user: LoggableProperties) {
        if (isOpen) {
            logger.identify(user)
        } else {
            enqueue(properties: user)
        }
    }

    func enqueue(_ event: LoggableEvent) {
        buffer.write(event)
    }

    func enqueue(properties: LoggableProperties) {
        buffer.write(properties)
    }
}
