# EventClient

[![pipeline status](https://gitlab.com/zedge-oss/zeppa/eventclient-swift/badges/master/pipeline.svg)](https://gitlab.com/zedge-oss/zeppa/eventclient-swift/-/commits/master)

Client for logging events and creating event taxonomy.

## Example usage

```
let taxman = Taxman(endpoint: "https://example.net")
        
var ourGreatTaxonomy = Taxonomy(appId: "amazingapp.ios")
try! ourGreatTaxonomy.update(properties: TestEvent(event: .OPEN_APP))
taxman.update(taxonomy: ourGreatTaxonomy).done({ (promise) in
  let (data, response) = promise
  XCTAssertTrue(String(data: data, encoding: .utf8)!
    .contains("successfully updated"))
    expect.fulfill()
})
waitForExpectations(timeout: 10, handler: nil)
```